# Projeto formulário de contato Empresa XYZ
## Exercício 03 

> Crie um formulário de contato para o Site da empresa XYZ. Campos: *Nome, *Email, *Empresa, *Telefone,  *Motivo do contato(Comercial, Reclamação, Sugestão, Cancelamento de Assinatura, Dúvida, Outros), *Seu Comentário(Permitir múltiplas linhas de digitação e um máximo de 1000 caracteres).
**Utilize Placeholder no lugar de Label
**Botão: Enviar
